﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    private float nextShot = 0f;

    protected override void Start ()
    {
        base.Start();

        StartCoroutine(CameraFollowPlayer());
	}

    IEnumerator CameraFollowPlayer ()
    {
        Vector3 cameraVelocity = Vector3.zero;
        while (true)
        {
            Vector3 cameraPos = Camera.main.transform.position;
            Camera.main.transform.position = Vector3.SmoothDamp(cameraPos, new Vector3(transform.position.x, transform.position.y, cameraPos.z), ref cameraVelocity, .25f);
            yield return null;
        }
    }

    void Update ()
    {
        Vector2 movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.x = mousePos.x - transform.position.x;
        mousePos.y = mousePos.y - transform.position.y;

        // Unit.cs
        Move(movement);
        RotateTo(mousePos);

        if (Input.GetButton("Fire1") && Time.time > nextShot)
        {
            nextShot = Time.time + shotInterval;
            Shoot();
        }

        if (rb.velocity.sqrMagnitude > 5f && !thrusterAudio.isPlaying)
        {
            thrusterAudio.Play();
        }
        else if (rb.velocity.sqrMagnitude < 5f && thrusterAudio.isPlaying)
        {
            thrusterAudio.Stop();
        }

        thrusterAudio.pitch = (rb.velocity.sqrMagnitude * .01f);
    }
}

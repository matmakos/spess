﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour {

    public float speed = 20f;
    public float maxShotLife = 10f;

    private float lifetime = 0f;

	// Use this for initialization
	void Start () {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.x = mousePos.x - transform.position.x;
        mousePos.y = mousePos.y - transform.position.y;
        
        // 105 because the camera is 10 units in the sky so magnitude squared is 100 next to the ship sprite
        if (mousePos.sqrMagnitude > 105f)
            transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg - 90f));
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        lifetime += Time.deltaTime;
        transform.position += transform.up * Time.deltaTime * speed;

        if (lifetime > maxShotLife)
            Destroy(gameObject);
	}
}

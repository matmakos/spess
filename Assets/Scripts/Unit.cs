﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Unit : MonoBehaviour
{
    public int maxHealth;
    public float speed;
    public float maxSpeed;
    public float shotInterval = .25f;
    public GameObject weapon;
    public AudioSource thrusterAudio;
    public AudioSource weaponAudio;

    protected Rigidbody2D rb;
    protected List<Transform> gunpoints = new List<Transform>();

    protected void Move(Vector2 force)
    {
        rb.AddForce(force * speed);
        rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxSpeed);
    }

    protected void RotateTo(Vector2 target)
    {
        // This function rotates with the X (blue) axis pointinig "forward" so we need to substract 90 degress to get correct rotation
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, Mathf.Atan2(target.y, target.x) * Mathf.Rad2Deg - 90f));
    }

    protected virtual void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        //thrusterAudio = transform.Find("ThrusterAudio").GetComponent<AudioSource>();
        //weaponAudio = transform.Find("WeaponAudio").GetComponent<AudioSource>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform gunpoint = transform.Find(string.Format("Gunpoint{0}", i));

            if (gunpoint != null)
                gunpoints.Add(gunpoint);
        }
    }

    protected virtual void Shoot ()
    {
        int gunpointUsed = Random.Range(0, gunpoints.Count);
        gunpoints[gunpointUsed].GetComponent<ParticleSystem>().Emit(8);

        Instantiate(weapon, gunpoints[gunpointUsed].position, transform.rotation);
        weaponAudio.Play();
    }
}

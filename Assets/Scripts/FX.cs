﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FX : MonoBehaviour
{
    private bool beingDisplayed;

    //protected GameObject fxObject;
    //protected SpriteRenderer spriteRenderer;

    public bool Displayed
    {
        get { return beingDisplayed; }
        set { beingDisplayed = value; }
    }

    protected virtual void Start ()
    {
        //fxObject = GetComponent<GameObject>();
        //spriteRenderer = GetComponent<SpriteRenderer>();
	}

    public abstract void ChangeOpacity(float alpha);
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Meteor : MonoBehaviour
{
    public bool small = false;

    private Rigidbody2D rb;
    private GameObject[] tiles;
    private int isColliding = 0;

    void OnMouseOver ()
    {
        Cursor.SetCursor(Manager.mouseOverCursor, Manager.cursorHotspot, CursorMode.Auto);
    }

    void OnMouseExit ()
    {
        Cursor.SetCursor(Manager.defaultCursor, Manager.cursorHotspot, CursorMode.Auto);
    }

    void OnCollisionEnter2D (Collision2D other)
    {
        Debug.Log("Player bumped into a meteor!");
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (isColliding == 0)
        {
            isColliding++;
            if (other.tag == "Lasers")
            {
                Destroy(other.gameObject);

                if (small)
                {
                    Destroy(gameObject);
                    return;
                }

                Vector2 newPos = new Vector2(Random.Range(transform.position.x - 1, transform.position.x + 1), Random.Range(transform.position.y - 1, transform.position.y + 1));
                GameObject newMeteor = Instantiate(tiles[Random.Range(0, tiles.Length)], newPos, Quaternion.Euler(new Vector3(0f, 0f, Random.Range(0f, 360f))));
                newMeteor.transform.localScale = new Vector3(.75f, .75f, 1f);
                newMeteor.GetComponent<Meteor>().small = true;

                newPos = new Vector2(Random.Range(transform.position.x - 1, transform.position.x + 1), Random.Range(transform.position.y - 1, transform.position.y + 1));
                newMeteor = Instantiate(tiles[Random.Range(0, tiles.Length)], newPos, Quaternion.Euler(new Vector3(0f, 0f, Random.Range(0f, 360f))));
                newMeteor.transform.localScale = new Vector3(.75f, .75f, 1f);
                newMeteor.GetComponent<Meteor>().small = true;

                Destroy(gameObject);
            }
        }
    }

    void OnTriggerExit2D (Collider2D other)
    {
        isColliding = 0;
    }

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();

        rb.AddTorque(Random.Range(-10f, 10f));
        tiles = GameObject.Find("GameManager").GetComponent<Map>().meteorTiles;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}

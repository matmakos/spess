﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrusterFX : FX
{
    private SpriteRenderer fxRenderer;

	protected override void Start ()
    {
        base.Start();
        fxRenderer = gameObject.GetComponent<SpriteRenderer>();
	}

    public override void ChangeOpacity(float alpha)
    {
        if (alpha >= 0 && alpha <= 1)
            fxRenderer.color = new Color(fxRenderer.color.r, fxRenderer.color.g, fxRenderer.color.b, alpha);
    }
}

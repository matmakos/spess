﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Map : MonoBehaviour
{
    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count(int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    class ArrayPoint
    {
        // Kinda like Vector2 but only to store indices to use with jagged arrays
        public int x;
        public int y;

        public ArrayPoint(int newX, int newY)
        {
            x = newX;
            y = newY;
        }
    }

    class Entity
    {
        // This class stores position in world coordinates & tile information for use with map array
        public Vector2 position;
        public Thing tile;

        public Entity(Vector2 pos, Thing t)
        {
            position = pos;
            tile = t;
        }
    }

    enum Thing
    {
        Void, Meteor, Enemy
    }

    public int columns = 100;
    public int rows = 100;
    public GameObject playerPrefab;
    public GameObject[] bgTiles;
    public GameObject[] enemyTiles;
    public GameObject[] meteorTiles;
    public Count meteorCount = new Count(5, 16);
    public Count enemyCount = new Count(0, 4);
    public Count starCount = new Count(50, 70);

    private Transform bgHolder;
    private Transform meteorHolder;
    private Entity[][] things;
    private Vector2 tileSize;

    void CalculateTileSize ()
    {
        // In world units
        tileSize = bgTiles[0].GetComponent<SpriteRenderer>().size;
    }

    void InitialiseList ()
    {
        // Initalize a jagged array of entities that will be randomized & spawned according to their enum type
        things = new Entity[columns][];

        for (int i = 0; i < things.Length; i++)
        {
            things[i] = new Entity[rows];
        }
    }

    void MapSetup ()
    {
        // Fill the map array with background tiles & create game objects to hold spawned tiles to reduce clutter
        bgHolder = new GameObject("Background").transform;
        meteorHolder = new GameObject("Meteors").transform;

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                things[x][y] = new Entity(new Vector2(tileSize.x * x, tileSize.y * y), Thing.Void);
            }
        }
    }

    ArrayPoint RandomPosition ()
    {
        return new ArrayPoint(Random.Range(0, columns), Random.Range(0, rows));
    }

    ArrayPoint GetArrayIndexesFromWorldPoint (Vector2 point)
    {
        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                if (things[x][y].position == point)
                    return new ArrayPoint(x, y);
            }
        }
        return new ArrayPoint(0, 0);
    }

    void InstantiateMapArray ()
    {
        // Create objects based on enum values in the array
        int starTilesSpawned = 0;
        int starTilesToSpawn = Random.Range(starCount.minimum, starCount.maximum);

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                switch (things[x][y].tile)
                {
                    case Thing.Void:
                        int chance = Random.Range(0, 2);
                        if (starTilesSpawned < starTilesToSpawn && chance == 1)
                        {
                            Instantiate(bgTiles[Random.Range(0, bgTiles.Length)], things[x][y].position, Quaternion.identity, bgHolder);
                            starTilesSpawned++;
                        }
                        break;
                    case Thing.Meteor:
                        Instantiate(meteorTiles[Random.Range(0, meteorTiles.Length)], things[x][y].position, Quaternion.identity, meteorHolder);
                        break;
                    case Thing.Enemy:
                        //Debug.Log(string.Format("Spawned enemy at x: {0} y: {1}", things[x][y].position.x, things[x][y].position.y));
                        break;
                }
            }
        }
    }

    void LayoutMap ()
    {
        for (int i = 0; i <= Random.Range(meteorCount.minimum, meteorCount.maximum); i++)
        {
            ArrayPoint randomPoint = RandomPosition();
            if (things[randomPoint.x][randomPoint.y].tile == Thing.Void)
                things[randomPoint.x][randomPoint.y].tile = Thing.Meteor;
        }

        for (int i = 0; i <= Random.Range(enemyCount.minimum, enemyCount.maximum); i++)
        {
            ArrayPoint randomPoint = RandomPosition();
            if (things[randomPoint.x][randomPoint.y].tile == Thing.Void)
                things[randomPoint.x][randomPoint.y].tile = Thing.Enemy;
        }
    }

    public void SetupMap (int level)
    {
        CalculateTileSize();

        InitialiseList();

        MapSetup();

        LayoutMap();

        InstantiateMapArray();

        Instantiate(playerPrefab, new Vector2(0f, 0f), Quaternion.identity);
    }
}

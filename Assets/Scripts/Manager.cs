﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{
    public static Manager instance = null;
    public Texture2D[] cursors;
    public static Texture2D defaultCursor;
    public static Texture2D mouseOverCursor;
    public static Texture2D enemyShipCursor;
    public static Texture2D enemyBaseCursor;
    public static Vector2 cursorHotspot = new Vector2(18f, 18f);

    private Map map;

    /* Make it so the BG tile(s) are randomly generated on the big (finite) board - DONE
     * Make random asteroids pop up near player - STATIC SPAWNS SO FAR, DYNAMIC SPAWNING IS EZ
     * Player is able to shoot and mine resources from asteroids, breaking them into smaller chunks
     * Points-of-interest (POI) spawn randomly (not too often) around asteroids (bases, loot chests etc.)
     * Enemies either spawn randomly or roam POI
     * Bosses spawn after a condition is met - score, time?
     * */
    void Start()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        map = GetComponent<Map>();
        defaultCursor = cursors[0];
        mouseOverCursor = cursors[1];
        enemyShipCursor = cursors[2];
        enemyBaseCursor = cursors[3];

        Cursor.SetCursor(defaultCursor, cursorHotspot, CursorMode.Auto);

        InitGame();
    }

    void InitGame()
    {
        map.SetupMap(1);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper
{
    public static bool IsAccelerating(Vector2 oldVelocity, Vector2 newVelocity)
    {
        return newVelocity.sqrMagnitude > oldVelocity.sqrMagnitude ? true : false;
    }
}
